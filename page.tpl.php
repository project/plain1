<?php
/**
* This snippet correct Doctype for Firefox/Opera and XML complient browser
*/
if(stristr($_SERVER["HTTP_ACCEPT"],"application/xhtml+xml")){
//	header("Content-Type: application/xhtml+xml; charset=UTF-8");
//	print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">');
	header("Content-Type: text/html; charset=UTF-8");
	print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
}
else {
	header("Content-Type: text/html; charset=UTF-8");
	print('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">');
}
/**
* This snippet creates a <body> class and id for each page.
*
* - Class names are general, applying to a whole section of documents (e.g. admin or ).
* - Id names are unique, applying to a single page.
*/
// Remove any leading and trailing slashes.
$uri_path = trim($_SERVER['REQUEST_URI'], '/');
// Split up the remaining URI into an array, using '/' as delimiter.
$uri_parts = explode('/', $uri_path);
// If the first part is empty, label both id and class 'main'.
if ($uri_parts[0] == '') {
    $body_id = 'main';
    $body_class = 'main';
}
else {
    // Construct the id name from the full URI, replacing slashes with dashes.
    $body_id = str_replace('/','-', $uri_path);
    // Construct the class name from the first part of the URI only.
    $body_class = $uri_parts[0];
}
/**
* Add prefixes to create a kind of protective namepace to prevent possible
* conflict with other css selectors.
*
* - Prefix body ids with "page-"(since we use them to target a specific page).
* - Prefix body classes with "section-"(since we use them to target a whole sections).
*/
$body_id = 'page-'.$body_id;
$body_class = 'section-'.$body_class;
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>    ">
    <head>
        <title>
            <?php print $title ?>
        </title>
       	<!-- to correct the unsightly Flash of Unstyled Content. http://www.bluerobot.com/web/css/fouc.asp -->
    	<script type="text/javascript"></script>
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <?php print $head ?>
        <?php print $styles ?>
    </head>
    <body <?php
        print "class=\"$body_class sb-$layout\" id=\"$body_id\"";
        print theme("onload_attribute"); ?>
        >
        <div id="top-nav">
            <?php if (is_array($primary_links)) : ?>
            <?php $i=0; ?>
            <ul id="primary">
                <?php foreach ($primary_links as $link): ?>
                <?php $i=$i+1; ?>
                <?php if ($i==count($primary_links)) print ('<li class="last">');
                else print ('<li>');
                ?>
                <?php print $link?></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
            <?php if (is_array($secondary_links)) : ?>
            <ul id="secondary">
                <?php foreach ($secondary_links as $link): ?>
                <li>
                <?php print $link?></li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
        <div id="page_wrapper">
            <div id="header">
                <?php if ($logo) : ?>
                <div id="sitelogo">
                <a href="<?php print url() ?>" title="Index Page">
                <img src="<?php print($logo) ?>" alt="Logo" /></a>
                </div>
                <?php endif; ?>
                <?php if ($search_box): ?>
                    <div id="search">
                        <form action="<?php print url("search") ?>" method="post">
                        <input class="form-text" type="text" size="15" value="" name="keys" />
                        <input class="form-submit" type="submit" value="<?php print t("Search")?>" />
                        </form>
                    </div>
                <?php endif; ?>
                <?php if ($site_name) : ?>
                <h1 id="site-name">
                    <a href="<?php print url() ?>" title="Index Page">
                        <?php print($site_name) ?></a></h1>
                <?php endif;?>
                <?php if ($site_slogan) : ?>
                <div id="site-slogan">
                    <?php print($site_slogan) ?>
                </div>
                <?php endif;?>
                <?php $mission = theme_get_setting('mission', false); ?>
                <?php if ($mission != ""): ?>
                <p id="mission">
                    <?php print $mission ?>
                </p>
                <?php endif; ?>
                <br class="clear" />
            </div>
            <div id="wrapper">
                <?php if ($sidebar_left != ""): ?>
                <div id="sidebar-left">
                    <?php print $sidebar_left ?>
                </div>
                <?php endif; ?>
                <div id="main-content" class="content <?php echo $layout;?>">
                    <div class="w1"><div class="w2"><div class="w3"><div class="w4">
                            <?php if ($breadcrumb != ""): ?>
                            <?php print $breadcrumb ?>
                            <?php endif; ?>
                            <?php if ($title != ""): ?>
                            <h2 class="content-title">
                                <?php print $title ?></h2>
                            <?php endif; ?>
                            <?php if ($tabs != ""): ?>
                            <?php print $tabs ?>
                            <?php endif; ?>
                            <?php if ($help != ""): ?>
                            <p id="help">
                                <?php print $help ?>
                            </p>
                            <?php endif; ?>
                            <?php if ($messages != ""): ?>
                            <div id="message">
                                <?php print $messages ?>
                            </div>
                            <?php endif; ?>
                            <!-- start main content -->
                            <?php print($content) ?>
                            <br />
                            <!-- end main content -->
                    </div></div></div></div>
                </div>
                <?php if ($sidebar_right != ""): ?>
                <div id="sidebar-right">
                    <?php print $sidebar_right ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div id="footer">
            <div class="w1">
                <div class="w2">
                    <?php if ($footer_message) : ?>
                    <p>
                        <?php print $footer_message;?>
                    </p>
                    <?php endif; ?>
                    <!--            <p>
                    design <a title="OSWD design work" href="http://oswd.org/userinfo.phtml?user=snop">snop</a> + photo <a title="stock.xchng" href="http://www.sxc.hu/browse.phtml?f=profile&amp;l=plasticboy&amp;p=1">plasticboy</a>   = valid <a title="validate XHTML" href="http://validator.w3.org/check?uri=referer">XHTML</a> &amp; <a title="validate CSS" href="http://jigsaw.w3.org/css-validator">CSS</a>
                    </p>
                    -->
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php print $closure;?>
    </body>
</html>
